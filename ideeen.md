
# Inhoudsopgave

- Wat is vim?
- Waarom vim?
  - Het is op Unix-systemen overal geinstalleerd.
  - Je kunt efficient je bedoelingen toepassen in vim.

- Opstarten, bewegen en afsluiten
  (h,j,k,l, :q, :q!)
- Insert mode
  (i, <ESC>)
- Write
  (:w)

Feestje, op dit punt kun je het als normale teksteditor gebruiken.

Vanaf nu zul je sneller gaan typen dan je kunt denken.

- Motions
  (w, b, e, ^, $)
- Deleten
  (x, dd)
- Combineer commando en motion
  (dw, d$)
- Undo, redo
  (u, ^R)
- Herhaal (commando + motion?)
  (1, 2, ...)
- Yank, paste
  (y, p, P)

- En veel meer...


## Nog niet geclassificeerd:

- `` (move to last marker)
- o, O (create newline below, above)
- { } (move between paragraphs)
- cc of S (substitute line)
- /, (?), n, N
- f, t
- .


Noem op het eind de vimtutor.
Deze kost veel tijd en kan voor mensen nuttig zijn om alles wat behandeld is, te herhalen.

# Overige opmerkingen

Laat de link openbaar beschikbaar zijn met alle info.

Voeg een bonus toe met extra motions en commands.



