# Inleiding

## Presentatie

Even een presentatie geven wat Vim precies voor editor is en waarom je het zou
gebruiken.

## Vim Installeren

We moeten een snelle en makkelijke manier bedenken waarmee iedereen Vim
geinstalleerd kan hebben. Hierbij zijn OSX en de meeste Linux distro's geen
probleem, want daar staat 't meestal standaard op. Het is vooral tricky als het
om Windows gaat, vooral omdat het altijd iets meer geklooi is met terminals
en dingen op je PATH krijgen.

Verder moeten we het bestand met de opgaven beschikbaar maken. Dit kan
natuurlijk via de A-Es website, maar ook via een Git repo, of met pastebin of
iets dergelijks.

# Opgaven

De opgaven zijn gemodelleerd naar die van `vimtutor`. Het hele bestand bestaat
uit lessen, die weer uit losse opdrachten bestaan. Tussen de opdrachten door
staan ook stukken tekst om bewerkingen op te oefenen.

Hier volgt de volgorden van commando's en concepten die we willen behandelen.
De kopjes hier hoeven niet één-op-één overeen te komen met de lessen en de
opdrachten.

## Notatie

Misschien is het een goed idee om even aan te geven hoe we bepaalde
toetsencombinaties gaan opschrijven. Namelijk: gewoon letters, hoofdletters
wanneer met shift, `<` en `>` voor speciale toetsen, e.g. `<ESC>`, `C-` voor
CTRL-combinaties. Dit omdat Vim zelf ook zulke notatie gebruikt in docs enzo,
dus het is handig om daar consistent mee te zijn.

## Opstarten, bewegen en afsluiten (pijltjes, `hjkl`, `:q`, `:q!`)

Instructies voor opstarten moeten op de slides, want bootstrapping probleem.
(Hoe kan je het bestand met instructies openen als je de instructies nog niet weet?)

Bewegen gaat in eerste instantie met de pijltjestoetsen; HJKL worden wel
genoemd als mogelijk alternatief (omdat je je handen dan minder hoeft te
bewegen).

`:q` om af te sluiten, en `:q!` voor force-quit als je wijzigingen niet wilt
opslaan.

## Insert mode (i, `<ESC>`)

`i` om insert mode in te gaan, `<ESC>` om eruit te gaan.

## Undo, redo (u, ^R)

`u` om 1 stap undo te doen, `<C-R>` om 1 stap redo te doen.

Misschien opmerken dat undo per command gaat, dus als je iets insert en dan
undo doet, gaat dat helemaal weg. Dit is in de praktijk meestal niet erg omdat je toch
niet grote stukken tekst in 1 keer insert.

## Write (:w)

`:w` om het bestand op te slaan.

Misschien ook een goed punt om aan te geven dat `:` een soort commandline is.

(Eventueel ook `:wq` en `:w!`?)

## Motions (w, b, e, ^, $)

`w` om een *w*oord vooruit te gaan, `b` om een woord achteruit te gaan
(*b*ackwards), `e` om naar het *e*inde van een woord te gaan.

`^` om naar het begin (zonder witruimte) van een regel te gaan, `$` naar het
einde.

## Deleten (x, dd)

`x` om een enkele character te deleten, `dd` voor een hele regel.

## Combineer commando en motion (dw, d$)

`dw` om vanaf de cursor tot het volgende woord te deleten, `d$` om tot het
einde van de regel te deleten.

`db`, `de`, `d^`

## Reflectie: de 'grammatica' van Vim

Er even bij stil staan dat alle normal-mode commando's een bepaalde vorm hebben.

Namelijk, mogelijk een (`d`, `c`, etc.), en so wie so een beweging (`w`, `$`,
etc.).

Uitzondering (maar handig) is dat het herhalen van een operatie die operatie op
de hele regel uitvoert, e.g. `dd`.

## Wijzigen (`c`)

`c` is eigenlijk een afkorting voor `d` gevolgd door `i`, maar het is heel
handig in combinatie met bewegingen.

`cw` om een woord te veranderen, `cc` om een regel te veranderen.

## Herhaal (commando + motion?) (1, 2, ...)

Een getal n gevolgd door een commando herhaalt het commando n keer.

`5u` gaat vijf stappen terug. `5dd` verwijdert 5 regels (omlaag).

Een getal kan ook voor een beweging.

`5w` beweegt de cursor vijf woorden vooruit. `5j` beweegt de cursor vijf regels
omlaag. `d5w` verwijdert 5 woorden.

Merk op dat het soms niet uitmaakt: `5dw` doet (ongeveer?) hetzelfde als `d5w`.

## Yank, paste (y, p, P)

(Misschien eerst visual mode?)

## Text Objects (i/a, w/)/")

(Misschien overbodig/te veel?)

Soms is het onhandig om je cursor precies op de juiste plek te krijgen en dan
precies de juiste motion te kiezen. _Textobjecten_ zijn hier een goede
oplossing voor. Je kan ze zien als motions die 'heen en weer' gaan ipv. één
richting.

Een textobject is een combinatie van `i` of `a` gevolgd door iets wat de
delimiters aangeeft (`w`, `)`, `"`, etc.).

`diw` verwijdert het woord waar de cursor op staat, maar niet de spaties om dat
woord heen; `daw` wel. `di)` verwijdert alles binnen een paar haakjes, `da)`
verwijdert ook de haakjes.

